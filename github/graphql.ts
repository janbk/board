
export const GQL_LOAD_ISSUES = `
query GetAll($repo: ID!) {
  project(fullPath: $repo) {
    name
    wp: issues(labelName: "WP") {
      nodes {
        ...issueData
      }
    }
    wi: issues(labelName: "WI") {
      nodes {
        ...issueData
      }
    }
    labels {
      nodes {
        title
        id
      }
    }
  }
}

fragment issueData on Issue {
  iid
  title
  humanTimeEstimate
  timeEstimate
  labels {
    nodes {
      title
      description
    }
  }
  blockedByCount
  subscribed
  reference
  milestone {
    id
  }
  webUrl
  weight
}
`;

export const GQL_CREATE_ISSUES = `
mutation createIssue($repo: ID!, $title: String!, $labels: [String!]) {
  createIssue(input: {projectPath: $repo, title: $title, labels: $labels}) {
    clientMutationId
  }
}
`;

export const GQL_UPDATE_ISSUE = `
mutation updateIssue($repo: ID!, $issue: String!, $title: String, $labelIds: [ID!]){
  updateIssue(input:{projectPath: $repo, iid: $issue, title: $title, labelIds: $labelIds}) {
    clientMutationId
  }
}
`;

export const GQL_CLOSE_ISSUE = `
mutation updateIssue($repo: ID!, $issue: String!){
  updateIssue(input:{projectPath: $repo, iid: $issue, stateEvent: "CLOSE"}) {
    clientMutationId
  }
}
`;
