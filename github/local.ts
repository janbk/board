import useSWR, { mutate } from 'swr';

export const readStorage = async (
    key: string
) => {
    return await localStorage.getItem(key);
};

export const useConfig = (key: string) => useSWR(key, () => readStorage(key));

export const writeStorage = async (
    key: string,
    value: string
) => {
    localStorage.setItem(key, value);
    mutate(key, value, false);
};
