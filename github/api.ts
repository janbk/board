import useSWR from 'swr';

const API = 'https://gitlab.com/api/';

export const DEFAULT_HEADER: RequestInit = {
    redirect: "error",
    headers: {
        Authorization: "Bearer " + process.env.NEXT_PUBLIC_GITLAB_AUTH_TOKEN,
        'Content-Type': 'application/json; charset=utf-8'
    }
};

export const DEFAULT_GRAPHQL_HEADER: RequestInit = {
    ...DEFAULT_HEADER,
    method: "POST"
};

export const fetchGraphQl = async (
    query: string, variables?: KeyValue
) => {
    const response = await fetch(API + 'graphql', {
        ...DEFAULT_GRAPHQL_HEADER,
        body: JSON.stringify({ query: query, variables: variables ?? null })
    });
    const data = await response.json();
    return data;
};

interface KeyValue {
    [key: string]: string | string[];
}

export const useGraphQl = (key: string, text: string, variables?: KeyValue) => useSWR(key, () => fetchGraphQl(text, variables));

export const fetchIssues = async (
) => {
    const response = await fetch(API + 'repos/Janbk/page/issues', {
        ...DEFAULT_HEADER,
    });
    const data = await response.json();
    return data;
};
