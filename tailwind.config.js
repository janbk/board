module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      colors: {
        'blue': '#5e81ac',
        'purple': '#b48ead',
        'pink': '#b48ead',
        'orange': '#d08770',
        'green': '#a3be8c',
        'yellow': '#ebcb8b',
        'red': '#bf616a',
        'gray-dark': '#2e3440',
        'gray': '#3b4252',
        'gray-light': '#4c566a',
        nord: {
          0: '#2E3440',
          1: '#3B4252',
          2: '#434C5E',
          3: '#4C566A',
          4: '#D8DEE9',
          5: '#E5E9F0',
          6: '#ECEFF4',
          7: '#8FBCBB',
          8: '#88C0D0',
          9: '#81A1C1',
          10: '#5E81AC',
          11: '#BF616A',
          12: '#D08770',
          13: '#EBCB8B',
          14: '#A3BE8C',
          15: '#B48EAD'
        },
        night: {
          0: '#2E3440',
          1: '#3B4252',
          2: '#434C5E',
          3: '#4C566A'
        },
        snow: {
          0: '#D8DEE9',
          1: '#E5E9F0',
          2: '#ECEFF4'
        },
        frost: {
          0: '#8FBCBB',
          1: '#88C0D0',
          2: '#81A1C1',
          3: '#5E81AC'
        },
        aurora: {
          11: '#BF616A',
          12: '#D08770',
          13: '#EBCB8B',
          14: '#A3BE8C',
          15: '#B48EAD'
        }
      }
    }
  },
  plugins: [require('@tailwindcss/forms')]
}
