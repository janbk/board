import NextDocument, { Html, Head, Main, NextScript } from "next/document"

export default class Document extends NextDocument {
  render() {
    return (
      <Html lang='de'>
        <Head />
        <body className='dark:bg-night-0 bg-snow-0 text-night-0 dark:text-snow-0 mb-8'>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
