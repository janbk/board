import { NextPage } from 'next'
import { useConfig } from '../github/local';

const Settings: NextPage = () => {
  const repo = useConfig('__repo');
  const key = useConfig('__key');

  return <div className='container'>
    <h1 className='text-6xl my-8'>
      Settings
    </h1>
  </div>
}

export default Settings
