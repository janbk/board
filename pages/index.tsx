import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Reorder } from 'framer-motion';
import { useCallback } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { mutate } from 'swr';
import Button from '../components/button';
import Card from '../components/card';
import Group from '../components/gitlab/group';
import Issue from '../components/gitlab/issue';
import Icon from '../components/icon';
import { fetchGraphQl, useGraphQl } from '../github/api';
import { GQL_LOAD_ISSUES, GQL_CREATE_ISSUES, GQL_UPDATE_ISSUE } from '../github/graphql';

export default function Issues() {
  const { data } = useGraphQl('issues', GQL_LOAD_ISSUES, {
    repo: "janbk/net"
  });

  const labels = data?.data?.project?.labels.nodes;
  const labelWi = labels?.find((l: any) => l.title === "WI");

  const createIssue = useCallback(async (evt: React.MouseEvent<HTMLButtonElement>) => {
    evt.preventDefault();
    await fetchGraphQl(GQL_CREATE_ISSUES, {
      repo: "janbk/net",
      title: "New Workpackage",
      labels: ["WP"]
    });
    await mutate('issues');
  }, []);

  const moveIssue = useCallback(async (issue: string, destination: string) => {
    const lblName = `WP${ destination }`;
    const label = labels?.find((l: any) => l.title === lblName);
    if (label) {
      await fetchGraphQl(GQL_UPDATE_ISSUE, {
        repo: "janbk/net",
        issue: issue,
        labelIds: [labelWi.id, label.id]
      });
      await mutate('issues');
    }
  }, [labels, labelWi])

  const onDragEnd = useCallback((result) => {
    const { source, destination } = result;
    console.log(result, source, destination)

    // dropped outside the list
    if (!destination) {
      return;
    }
    const sInd = source.droppableId;
    const dInd = destination.droppableId;

    if (sInd === dInd) {
      //Reorder
    } else {
      moveIssue(result.draggableId, dInd);
    }
  }, [moveIssue]);

  return (<>
    <h1 className='text-6xl my-8'>
      {data?.data?.project?.name}
    </h1>
    <h2 className='text-xl my-8'>
      Issues
    </h2>
    <DragDropContext onDragEnd={onDragEnd}>
      <div className='pr-2 space-y-4'>
        {data?.data?.project?.wp.nodes && data.data.project.wp.nodes
          .filter(
            (issue: any) => issue.labels?.nodes.some((lbl: any) => lbl.title === "WP")
          )
          .map((WpIssue: any) => <Group key={WpIssue.reference} {...WpIssue}>
            {(provided, snapshot) => (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                {data?.data?.project?.wi.nodes && data.data.project.wi.nodes
                  .filter(
                    (issue: any) => issue.labels?.nodes.some((lbl: any) => lbl.title === `WP${ WpIssue.reference }`)
                  )
                  .map((issue: any, index: number) => <Issue key={issue.reference} index={index} {...issue} />
                  )
                }
                {provided.placeholder}
              </div>
            )}
          </Group>
          )
        }
      </div>
    </DragDropContext>

    <h2 className='text-xl my-8'>
      Labels
    </h2>
    <div className='grid grid-cols-1 pr-2 md:grid-cols-2 xl:grid-cols-3 gap-6'>
      {labels && labels.map((label: any) => <Card key={label.id} >{label.title}</Card>)}
    </div>


    <Button
      aria-label="goto"
      onClick={createIssue}
      className='my-8'
    >
      <Icon icon={faPlus} />
    </Button>
  </>
  )
}

Issues.getLayout = function getLayout(page: React.ReactElement) {
  return <BlankLayout>{page}</BlankLayout>;
}
