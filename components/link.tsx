import React from "react";
import NextLink, { LinkProps } from 'next/link';

interface ExtendedLinkProps extends LinkProps {
    className?: string;
    onClick?: React.MouseEventHandler<HTMLAnchorElement>;
}

export const Link: React.FC<ExtendedLinkProps> = (props) => {
    const { className, onClick, ...linkProps } = props;
    return (
        <NextLink {...linkProps} >
            <a className={className} onClick={onClick}>
                {props.children}
            </a>
        </NextLink>
    );
}
