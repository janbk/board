import React from "react";
import classNames from "classnames";

interface BaseButtonProps {
    disabled?: boolean;
    className?: string;
}
interface ButtonProps extends BaseButtonProps {
    isOn?: boolean;
    onClick: React.MouseEventHandler<HTMLButtonElement>;
}

const Button: React.FC<ButtonProps> = (props) => {
    const { isOn, ...rest } = props;

    return (
        <button
            {...rest}
            type="button"
            aria-label="state"
            className={
                classNames(
                    "bg-nord-4 ring-transparent hover:ring-nord-8 ring-2 dark:bg-nord-2 dark:hover:ring-nord-8 p-3 border-nord-6 rounded-lg transition-colors h-14 w-14 z-40 hover:z-50",
                    props.className,
                )
            }
        >
            {props.children}
        </button >
    );
}

export default Button;
