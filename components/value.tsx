import React from "react";

interface ValueProps {
    value?: number;
    precision?: number;
    unit?: string;
    className?: string;
    isLoading?: boolean;
}

const Value: React.FC<ValueProps> = (props) => (
    <span className={props.className ?? ''}>
        {props.value !== undefined && props.value !== NaN && <>
            {props.value.toFixed(props.precision ?? 2)}
            <span>{props.unit}</span>
        </>}
    </span>
)

export default Value;
