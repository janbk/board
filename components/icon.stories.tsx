// icon.stories.ts|tsx

import React from 'react';
import '../styles/globals.css'

import { ComponentMeta } from '@storybook/react';
import Icon from './icon';
import { faGhost } from '@fortawesome/free-solid-svg-icons';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Icon',
  component: Icon,
} as ComponentMeta<typeof Icon>;

export const BasicIcon = () => <Icon icon={faGhost} />;
