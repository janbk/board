import { faPlus } from '@fortawesome/free-solid-svg-icons';
import React, { useCallback } from "react";
import { Droppable, DroppableProvided, DroppableStateSnapshot } from 'react-beautiful-dnd';
import { mutate } from 'swr';
import { fetchGraphQl } from '../../github/api';
import { GQL_CREATE_ISSUES } from '../../github/graphql';
import Button from '../button';
import Icon from '../icon';
import Input from './input';

interface GroupProps {
    className?: string;

    iid: string;
    title: string;
    webUrl: string;
    reference: string;
    children(provided: DroppableProvided, snapshot: DroppableStateSnapshot): React.ReactElement<HTMLElement>;
}

const Group: React.FC<GroupProps> = (props) => {
    const createItem = useCallback(async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        await fetchGraphQl(GQL_CREATE_ISSUES, {
            repo: "janbk/net",
            title: "New work package",
            labels: ["WI", `WP${ props.reference }`]
        });
        await mutate('issues');
    }, [props.reference]);

    return (
        <div
            className="border-2 border-nord-3 rounded-xl"
        >
            <Input
                name='title'
                iid={props.iid}
                value={props.title}
                className='text-xl ml-2 mt-2'
            />

            <div
                className='m-4 grid grid-cols-1 pr-2 md:grid-cols-2 xl:grid-cols-3 gap-6'
            >
                <Droppable droppableId={props.reference}>
                    {props.children}
                </Droppable>
            </div>
            <Button onClick={createItem}>
                <Icon icon={faPlus} />
            </Button>
        </div >
    );
}

export default Group;
