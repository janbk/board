import { faLink, faTrash } from '@fortawesome/free-solid-svg-icons';
import React, { useCallback } from "react";
import { mutate } from 'swr';
import { fetchGraphQl } from '../../github/api';
import { GQL_CLOSE_ISSUE, GQL_UPDATE_ISSUE } from '../../github/graphql';
import Button from '../button';
import Card from '../card';
import Icon from '../icon';
import Input from './input';
import { Draggable } from "react-beautiful-dnd";

interface IssueProps {
    className?: string;

    iid: string;
    title: string;
    webUrl: string;
    reference: string;
    index: number;
}

const Issue: React.FC<IssueProps> = (props) => {

    const deleteIssue = useCallback(async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        await fetchGraphQl(GQL_CLOSE_ISSUE);
        await mutate('issues');
    }, [GQL_CLOSE_ISSUE])

    return (
        <Draggable
            draggableId={props.iid}
            index={props.index}
        >
            {(provided, snapshot) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                >
                    <Card flex className={props.className} >
                        <Card.Header>
                            <Input
                                name='title'
                                iid={props.iid}
                                value={props.title}
                                className='text-xl'
                            />
                        </Card.Header>

                        <Card.Actions>
                            <Button
                                aria-label="goto"
                                onClick={(e) => window.open(props.webUrl, '_blank')}
                            >
                                <Icon icon={faLink} />
                            </Button>
                            <Button
                                aria-label="goto"
                                onClick={deleteIssue}
                            >
                                <Icon icon={faTrash} />
                            </Button>
                            <div className='absolute top-2 left-2 text-nord-3'>
                                {props.reference}
                            </div>
                        </Card.Actions>
                    </Card>
                </div>
            )}
        </Draggable>
    );
}

export default Issue;
