import { useCallback } from 'react';
import { mutate } from 'swr';
import { fetchGraphQl } from '../../github/api';
import { GQL_UPDATE_ISSUE } from '../../github/graphql';

interface InputProps {
    className?: string;

    iid: string;
    value: string;
    name: string;
}

const Input: React.FC<InputProps> = (props) => {
    const { iid, value, className, ...inputProps } = props;

    const updateIssue = useCallback(async (evt: React.FocusEvent<HTMLInputElement>) => {
        evt.preventDefault();
        console.log(evt.target.value)
        await fetchGraphQl(GQL_UPDATE_ISSUE,
            {
                repo: "janbk/net",
                issue: props.iid,
                [evt.target.name]: evt.target.value
            });
        await mutate('issues');
        // Clear input
        evt.target.value = '';
    }, [GQL_UPDATE_ISSUE, props.iid]);

    return <input
        className={props.className + ' p-2 font-serif grow border-transparent bg-transparent placeholder:text-snow-2 focus-visible:outline-none focus-visible:border-green border-b-2'}
        placeholder={value}
        onBlur={updateIssue}
        {...inputProps}
    />;
}

export default Input;
