import React from "react";

interface LogoProps {
    className?: string;
    width?: number;
    off?: boolean;
}

export const Logo: React.FC<LogoProps> = (props) => {
    return (
        <svg viewBox={`-10 -10 142 142`} width={props.width} xmlns="http://www.w3.org/2000/svg" className={props.className}>
            <path
                d="M 0 60 a 60 60 0 0 1 60 60 h 60 v -60 l -60 -60 z"
                stroke="currentColor"
                strokeWidth="12"
                fill="none"
            />

            {props.off !== false && <>
                <circle cy="120" r="35"
                    stroke="currentColor"
                    strokeLinecap="square"
                    strokeWidth="12"
                    fill="none"
                />
                <circle cy="120" r="15"
                    stroke="currentColor"
                    strokeLinecap="square"
                    strokeWidth="0"
                    fill="currentColor"
                />
            </>}
        </svg>
    );
}
