import React from "react";
import styles from './slider.module.css';

const Slider: React.FC<React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>> = (props) => {
    return (
        <div className="flex">
            <input {...props} type="range" className={styles.slider} />
            <output className="text-frost-0 ml-4">{props.value}</output>
        </div>
    );
}

export default Slider;
