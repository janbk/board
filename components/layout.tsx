import React from "react";

const Layout: React.FC = (props) => {

    return (
        <div className="container md:mx-auto px-3">
            {props.children}
        </div>
    );
}

export default Layout;
