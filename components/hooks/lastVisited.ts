import { useRouter } from "next/router";
import { useEffect } from "react";

export default function useLastVisited() {
    const router = useRouter();

    useEffect(() => {
        if (router.route === "/") {
            const lastPath = localStorage.getItem("last");
            if (lastPath) {
                router.replace(lastPath);
            }
        } else {
            localStorage.setItem("last", router.asPath);
        }
    }, [router]);

    return router;
}
