// card.stories.ts|tsx

import React from 'react';
import '../styles/globals.css'

import { ComponentMeta, ComponentStory } from '@storybook/react';

import Card from './card';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Card',
  component: Card,
} as ComponentMeta<typeof Card>;

export const EmptyCard = () => <Card />;

export const BasicCard = () => <Card>Hi</Card>;

const TemplateFullCard: ComponentStory<typeof Card> = (args) => <Card {...args}>
  <Card.Header>Lorem ipsum dolor</Card.Header>
  <Card.Body>
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
  </Card.Body>
</Card>;

export const FullCard = TemplateFullCard.bind({});
FullCard.args = {};

export const FullCardLocked = TemplateFullCard.bind({});
FullCardLocked.args = {
  isLocked: true
};

export const FullCardActive = TemplateFullCard.bind({});
FullCardActive.args = {
  isActive: true
};

export const FullCardActiveLocked = TemplateFullCard.bind({});
FullCardActiveLocked.args = {
  isLocked: true,
  isActive: true
};

/**
 * With Value Stories
 */
export const CardValue0 = TemplateFullCard.bind({});
CardValue0.args = {
  withValue: 0
};
export const CardValue1 = TemplateFullCard.bind({});
CardValue1.args = {
  withValue: 1
};
export const CardValue100 = TemplateFullCard.bind({});
CardValue100.args = {
  withValue: 100
};

export const CardValue100Locked = TemplateFullCard.bind({});
CardValue100Locked.args = {
  isLocked: true,
  withValue: 100
};
