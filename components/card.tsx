import classNames from "classnames";
import React from "react";

interface CardProps {
    flex?: boolean;
    isLocked?: boolean;
    isActive?: boolean;
    withValue?: number | boolean;
    color?: string;
    className?: string;
}

const Card: React.FC<CardProps> = (props) => {
    const withValue = props.withValue === true ? 100 : Number(props.withValue);
    return (
        <div
            className={classNames(
                props.className,
                "relative shadow bg-nord-6 dark:bg-nord-1 rounded-xl",
                {
                    "ring-red ring": props.isLocked,
                    "shadow-lg shadow-yellow": props.isActive
                }
            )}
        >
            <div
                className={classNames(
                    props.className,
                    "z-10 bg-snow-2 dark:bg-night-1 text-night-1 dark:text-snow-0 rounded-xl p-0 md:p-2 pl-6",
                    {
                        "flex flex-wrap gap-4": props.flex
                    }
                )}
            >
                {props.children}
                {props.withValue !== undefined &&
                    <div
                        className='absolute -z-10 -top-3 -right-2 left-0 h-8 rounded-lg bg-snow-2/10 dark:bg-night-3/25'
                    >
                        <div
                            className={(props.isLocked || props.color === undefined ? 'bg-red' : props.color) + ' rounded-lg h-full duration-700 transition-[width] shadow'}
                            style={{ width: `${ withValue }%` }}
                        />
                    </div>}
            </div >
        </div>
    );
}

interface CardHeaderProps {
    placeholder?: string;
    name?: string;
    onBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
}

const CardHeader: React.FC = (props) => {
    const { children, ...rest } = props;
    return (
        <div className='font-serif text-xl pl-3 pt-3  grow' {...rest}>
            {children}
        </div>
    );
}

const CardBody: React.FC = (props) => {
    return (
        <div className='basis-full pl-6 grow self-center pb-2'>
            {props.children}
        </div>
    );
}

const CardActions: React.FC = (props) => {
    return (
        <div className='bg-nord-4 dark:bg-nord-2 dark:ring-nord-2 dark:hover:ring-nord-8 border-nord-6 rounded-lg transition-colors'>
            {props.children}
        </div>
    );
}

Card.displayName = 'Card';

export default Object.assign(Card, {
    Header: CardHeader,
    Body: CardBody,
    Actions: CardActions
});
