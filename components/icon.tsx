import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome';
import React from "react";

const Icon: React.FC<FontAwesomeIconProps> = (props) => {
    return (
        <FontAwesomeIcon {...props} fixedWidth style={{ maxWidth: "2em" }} />
    );
}

export default Icon;
