// Slider.stories.ts|tsx

import React from 'react';
import '../styles/globals.css'

import { ComponentMeta } from '@storybook/react';
import Slider from './slider';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Slider',
  component: Slider,
} as ComponentMeta<typeof Slider>;

export const BasicSlider = () => <Slider />;
